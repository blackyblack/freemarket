Release 1.5

To use FreeMarket, you must first have NXT installed and have a NXT account with some NXT in it.

If you don't have it yet, download the latest Nxt Client. The Nxt Client is cross-platform, and you can get the latest 1.5.15 release here (https://bitbucket.org/JeanLucPicard/nxt/downloads/nxt-client-1.5.15.zip).

Unzip the Nxt Client in a location of your choice.

To install FreeMarket, download the FreeMarket here:
Windows Installer: [https://bitbucket.org/blackyblack/freemarket/downloads/FreeMarket-setup-win32-1.5.exe]
Windows portable app: [https://bitbucket.org/blackyblack/freemarket/downloads/FreeMarket-win32-1.5.zip]
Mac portable app: [https://bitbucket.org/blackyblack/freemarket/downloads/FreeMarket-mac-1.5.zip]
Cross-platfrom distributive: [https://bitbucket.org/blackyblack/freemarket/downloads/FreeMarket-dist-1.5.zip]

For better compatibility with Windows10/64bit OS try this versions:
Windows 64bit Installer: [https://bitbucket.org/blackyblack/freemarket/downloads/FreeMarket-setup-win64-1.5.exe]
Windows 64bit portable app: [https://bitbucket.org/blackyblack/freemarket/downloads/FreeMarket-win64-1.5.zip]

sha256:

a638ec849b2b39f1e33ab22d232516ff6bc9243e6df10b24b76d07d55176be06 *FreeMarket-dist-1.5.zip
de22f54ca0462cc13c0fd8669666cf33d9499e9fd7a3261ac0a57574fa4aed4a *FreeMarket-setup-win32-1.5.exe
a356234860b8b2772e9c2ea2b093264eec166917fac4c2b3690a304831bf5a27 *FreeMarket-setup-win64-1.5.exe
f4d6b02479dcafa0ffd1ed42cc6fab62129be5870dc0c50d0fe2704c760a68ee *FreeMarket-win32-1.5.zip
a372df18b58ee11b4e558fb730c077de9334916cf7936744fc87e531ea3ace67 *FreeMarket-win64-1.5.zip
c8fbab099d70ab9df4a3797ea5ac1c6f2e2de995821064dbc7df0c03171cfdc1 *FreeMarket-mac-1.5.zip

NXT token:

Nxt Addr : NXT-9HP3-8QJW-NY3Y-9QLLZ

Message :
a638ec849b2b39f1e33ab22d232516ff6bc9243e6df10b24b76d07d55176be06 *FreeMarket-dist-1.5.zip
de22f54ca0462cc13c0fd8669666cf33d9499e9fd7a3261ac0a57574fa4aed4a *FreeMarket-setup-win32-1.5.exe
a356234860b8b2772e9c2ea2b093264eec166917fac4c2b3690a304831bf5a27 *FreeMarket-setup-win64-1.5.exe
f4d6b02479dcafa0ffd1ed42cc6fab62129be5870dc0c50d0fe2704c760a68ee *FreeMarket-win32-1.5.zip
a372df18b58ee11b4e558fb730c077de9334916cf7936744fc87e531ea3ace67 *FreeMarket-win64-1.5.zip
c8fbab099d70ab9df4a3797ea5ac1c6f2e2de995821064dbc7df0c03171cfdc1 *FreeMarket-mac-1.5.zip

Token : 4avn6us1bo1601otsnet7gqc4uvok2sgf27i0l9ii3knr074jg7jehp6u7uovr83dj4a55ec9hvekfoc2ku53030gjf9ljfm9qpi2dpa5m4gajfh7fm2oa4f6bso14058i1khbpoitb5v5cftrmvm69g3mifcd69